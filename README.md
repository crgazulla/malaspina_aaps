# Malaspina AAPs

Code for running the analyses carried out in Gazulla _et al._ 2022, **Global diversity and distribution of aerobic anoxygenic phototrophs in the tropical and subtropical oceans**
https://doi.org/10.1111/1462-2920.15835

For the PDF version of the manuscript, doubts or questions you can find me here: lotaruiz@gmail.com :)
